const { ethers, upgrades } = require("hardhat");

async function main () {
  const gnosisSafe = '0x6E4bFE8cDd5C5e1A207b4751c4e962Ed110D14e4';

  console.log('Transferring ownership of ProxyAdmin...');
  // The owner of the ProxyAdmin can upgrade our contracts
  await upgrades.admin.transferProxyAdminOwnership(gnosisSafe);
  console.log('Transferred ownership of ProxyAdmin to:', gnosisSafe);
}

main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });