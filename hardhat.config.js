require("dotenv").config();

require("@nomiclabs/hardhat-etherscan");
require("@nomiclabs/hardhat-waffle");
require("hardhat-gas-reporter");
require("solidity-coverage");
require('@openzeppelin/hardhat-upgrades');
require('@openzeppelin/hardhat-defender');


// You need to export an object to set up your config
// Go to https://hardhat.org/config/ to learn more


module.exports = {
	defender: {
		apiKey: process.env.DEFENDER_TEAM_API_KEY,
		apiSecret: process.env.DEFENDER_TEAM_API_SECRET_KEY,
	},
	defaultNetwork: "binance",
	networks: {
		hardhat: {
		},
		binance: {
			url: "https://bsc-dataseed.binance.org/",
			accounts: [process.env.BSC_MAINNET_PRIVATE_KEY]
		}

	},
	solidity: {
		version: "0.8.10",
		settings: {
			optimizer: {
				enabled: true,
				runs: 200
			}
		}
	},
}
