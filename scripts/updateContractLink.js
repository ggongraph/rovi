const { defender } = require("hardhat");

async function main() {
	        const R91TokenChild = await ethers.getContractFactory("R91TokenChildBinance");
			console.log("Preparing proposal...");
			const proposal = await defender.proposeUpgrade("0xD02D45dF2D9E8eE28A15D199689aEfb1B4a74043", R91TokenChild);
			console.log("Upgrade proposal created at:", proposal.url);
}
main().catch((error) => {
	          console.error(error);
	          process.exitCode = 1;
});
