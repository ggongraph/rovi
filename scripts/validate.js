const { ethers, upgrades } = require("hardhat");

async function main() {
	const R91TokenChild = await ethers.getContractFactory("R91TokenChildBinance");
	await upgrades.validateUpgrade("0xD02D45dF2D9E8eE28A15D199689aEfb1B4a74043", R91TokenChild);
	
}

main();
