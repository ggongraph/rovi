const { ethers, upgrades } = require("hardhat");

async function main() {
	const R91TokenChild = await ethers.getContractFactory("R91TokenChildBinance");
	const r91TokenChild = await upgrades.deployProxy(R91TokenChild, ["0x86C80a8aa58e0A4fa09A69624c31Ab2a6CAD56b8"], { timeout: 0 });
	await r91TokenChild.deployed();
	console.log("R91TokenChildBinance deployed to:", r91TokenChild.address);
}

main();
